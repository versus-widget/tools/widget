import { useEffect, useState } from 'react'
import VsLabel from './components/VsLabel'
import HitEffect from './components/HitEffect'

import * as io from "socket.io-client"
import FightersBG from './components/FightersBG'
const api = "http://localhost:3000"
const socket = io.connect(api)

const App = () => {
  const [ classCount, setClassCount ] = useState(0)
  const [ functionalCount, setFunctionalCount ] = useState(0)
  const [ lastUser, setLastUser ] = useState('')

  const commands = {
    class: '/go class',
    functional: '/go functional',
  }

  socket.on('publish-vote', ({ user, command }) => {
    if (user && command) {
      if (command == commands.class)     setClassCount(classCount + 1)
      if (command == commands.functional) setFunctionalCount(functionalCount + 1)
      if (command == commands.functional || command == commands.class ) setLastUser(user)
    }
  })

  useEffect(() => {
    console.log({classCount, functionalCount})
  }, [classCount, functionalCount])

  return (
    <div className='app'>
      <FightersBG { ...{ classCount, functionalCount } }/>
      <div className="app__content">
        <HitEffect { ...{ classCount, functionalCount, lastUser } }/>
        <VsLabel { ...{ classCount, functionalCount } }/>
      </div>
    </div>
  )
}

export default App
