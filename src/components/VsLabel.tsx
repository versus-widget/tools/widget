import TextTransition, { presets } from 'react-text-transition';

// @ts-ignore
const VsLabel = ({ classCount, functionalCount }) => {
    return (
        <div className="vs-label">
            <div className="vs-label__fighter">
                <TextTransition className="count" springConfig={presets.wobbly}>
                    {classCount}
                </TextTransition>
                <span className="label">CLASS</span>
            </div>
            <div className="vs-label__vs-bubble">
                <span>VS</span>
            </div>
            <div className="vs-label__fighter">
                <TextTransition className="count" springConfig={presets.wobbly}>
                    {functionalCount}
                </TextTransition>
                <span className="label">FUNCTIONAL</span>
            </div>
        </div>
    )
}

export default VsLabel;