import { useEffect, useState } from 'react'
import posed from "react-pose";
import ryuImg from "../assets/img/ryu.png"
import akumaImg from "../assets/img/akuma.png"

const ShakePose = posed.div({
    shake: {
      applyAtEnd: { x: 0 },
      applyAtStart: { x: -10 },
      x: 0,
      transition: {
        type: "spring",
        stiffness: 1000,
        damping: 10,
        duration: 4
      }
    }
});

// @ts-ignore
const FightersBG = ({ classCount, functionalCount }) => {

    return (
        <div className="fighters-bg">
            <div className="center">
                <div className="ryu">
                    <ShakePose pose={["shake"]} poseKey={functionalCount}>
                            <img src={ryuImg} alt="ryu" />
                    </ShakePose>
                </div>
                <div className="akuma">
                    <ShakePose pose={["shake"]} poseKey={classCount}>
                            <img src={akumaImg} alt="ryu" />
                    </ShakePose>
                </div>
            </div>
        </div>
    )
}

export default FightersBG;