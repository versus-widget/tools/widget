import { useEffect, useState } from 'react'

// @ts-ignore
const HitEffect = ({ classCount, functionalCount, lastUser }) => {
    const [ classHit, setClassHit] = useState([])
    const [ functionalHit, setFunctionalHit] = useState([])
    

    useEffect(() => {
        // @ts-ignore
        classCount && setClassHit(prev => [...prev, classCount])
    }, [classCount])

    useEffect(() => {
        // @ts-ignore
        functionalCount && setFunctionalHit(prev => [...prev, functionalHit])
    }, [functionalCount])

    return (
        <div className="hit-effect">
            <div className="class-hit">
                {functionalHit.map(({value}) => (
                    <div key={value} className="hit">Hit by {lastUser.toUpperCase()}</div>
                ))}
            </div>
            <div className="functional-hit">
                {classHit.map(({value}) => (
                    <div key={value} className="hit">Hit by {lastUser.toUpperCase()}</div>
                ))}
            </div>
        </div>
    )
}

export default HitEffect;